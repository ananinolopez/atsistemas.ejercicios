
LAST FORM

<head>
    <title>Inicio de Sesión</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-metro.css">
    <meta charset="UTF-8">
</head>

@section('myform')
    <div class="w3-card-4 w3-left w3-display-middle w3-round-xxlarge" style="width:50%" >
        <div class="w3-container w3-metro-dark-blue w3-round-xxlarge w3-center" style="width:100%">
            <h2>Iniciar Sesión</h2>

        </div>
        <form class="w3-container" action="verifyname" method='POST'>
            @csrf
            <p>
                <label class="w3-dark"><b>Usuario</b></label>
                <input class="w3-input w3-border w3-light-gray" type="text" placeholder="Introduce el nombre de usuario" name="login" required>
            <p>
                <label class="w3-dark"><b>Clave</b></label>
                <input class="w3-input w3-border w3-light-gray" type="password" placeholder="Introduce la clave" name="password" required>
            <p>
                <button class="w3-btn w3-metro-dark-blue w3-round-xxlarge">Conectar</button></p>
        </form>
    </div>
@show


