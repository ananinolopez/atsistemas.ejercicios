<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/index', 'BlogController@index')->name('index');
Route::get('/show', 'BlogController@show')->name('show');
Route::get('/aux1', 'BlogController@aux1')->name('aux1');
Route::post('/verifyname', 'BlogController@verifyname')->name('verifyname');
Route::get('/verify', 'BlogController@verify')->name('verify');
Route::any('/form', 'BlogController@form')->name('form');
/*Route::get('usuario/{name}','BlogController@verify')*/
Route::get('/verifyname', 'BlogController@verifyname')->name('aux2');
Route::get('/database', 'ContactController@database')->name('database');
Route::get('/advform', 'PostController@fillingform')->name('advform');
Route::get('/adults', 'AdultsController@adults')->name('adults')->middleware('age');

?>

