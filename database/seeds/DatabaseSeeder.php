<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('posts')->insert([
            'id'=>rand(),
            'title'=>Str::random(10).'bla',
            'article'=>Str::random(10),
        ]);

        DB::table('comments')->insert([
        'id'=>rand(),
        'post_id'=>rand(),
        'email'=>Str::random(10).'@gmail.com',
            'comment'=>Str::random(20),
            'isResponse'=>0,
            'comment_id'=>rand(),
         ]);
    }


}

