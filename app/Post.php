<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //

    protected $fillable = ['name'];
    public $timestamps=true;
    const CREATED_AT='created_at';
    const UPDATE_AT='update_at';
    public function comment()
    {
        return $this->belongsToMany('App\Comment');
    }
}
