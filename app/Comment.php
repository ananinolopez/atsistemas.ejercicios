<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Comment extends Model
{
    //
    protected $table = 'comment';
    protected $primaryKey = 'id';

    protected $fillable = ['name'];
    public $timestamps=true;
    const CREATED_AT='created_at';
    const UPDATE_AT='update_at';

    public function post()
    {
        return $this->hasOne('App\Post','id','post_id');
    }
}
