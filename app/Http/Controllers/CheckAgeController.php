<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BeforeMiddleware
{
    public function handle($request, Closure $next)
    {
// Perform action
        $years= $request->input('years',16);
        if ($years<18){
            return redirect('adults');
        }

        return $next($request);
    }
}
